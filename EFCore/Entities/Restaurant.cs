﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCore.Entities
{
    public class Restaurant : Entity
    {
        
        public string Name { get; set; }

        public string Description { get; set; }

        public virtual List<Review> Reviews { get; set; }

        public virtual List<User> Owners { get; set; }

    }
}
