﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EFCore.Entities
{
    public class User : Entity
    {
        public string Name { get; set; }

        public virtual List<Restaurant> Restaurants { get; set; }

        public DateTime Date { get; set; }
    }
}