﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCore.Entities
{
    public class Review : Entity
    {
        public string Text { get; set; }

        public int Rating { get; set; }

        public DateTime CreatedAt { get; set; }


        public DateTime ModifiedAt { get; set; }

        [Required]
        public virtual User Reviewer { get; set; }

        [Required]
        public virtual Restaurant Restaurant { get; set; }
    }
}
