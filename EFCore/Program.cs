﻿using EFCore.Data;
using EFCore.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EFCore
{
    class Program
    {
        private static DatabaseContext _context;
        private static DatabaseContext _context2;

        static void Main(string[] args)
        {

            _context = new DatabaseContext();
            _context2 = new DatabaseContext();
            DataSeeder.Seed(_context);

            //AddReviewFromRestaurant();
            //AddReview();

            RemoveRestaurant();
        }

        private static void RemoveRestaurant()
        {
            User user = _context.Set<User>().FirstOrDefault(u => u.Id == 1);
            
            _context.Entry(user).Reload();
            


            _context.SaveChanges();
        }

        private static void AddReview()
        {
            Restaurant restaurant = _context.Restaurants.FirstOrDefault(r => r.Id == 1);
            User user = _context.Set<User>().First();

            Review review = new Review()
            {
                Restaurant = restaurant,
                Reviewer = user,
                Rating = 5,
                Text = "new review new review new review new review new review new review new review "
            };

            _context.Set<Review>().Add(review);

            _context.SaveChanges();
        }

        public static void AddReviewFromRestaurant()
        {
            User user = _context.Set<User>().First();

            Restaurant restaurant = _context.Restaurants.FirstOrDefault(r => r.Id == 1);
            Restaurant restaurant2 = _context.Restaurants.FirstOrDefault(r => r.Id == 2);

            restaurant.Reviews.Add(new Review()
            {
                Restaurant = restaurant,
                Rating = 4,
                Reviewer = user,
                Text = " newly created reviewnewly created reviewnewly created reviewnewly created reviewnewly created reviewnewly created reviewnewly created reviewnewly created reviewnewly created review"
            });

            _context.SaveChanges();
        }
    }
}
