﻿using EFCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCore.Data
{
    public class DataSeeder
    {
        public static void Seed(DatabaseContext context)
        {
            if (context.Database.EnsureCreated())
            {
                User yavor = new User() { Name = "yavor" };

                context.Users.Add(yavor);
                context.Users.Add(new User() { Name = "mitko" });

                Restaurant yavorsPlace = new Restaurant()
                {
                    Name = "Yavor's place",
                    Description = "Restaurant owned by Yavor",
                    Owners = new List<User>()
                    {
                        yavor
                    }
                };

                context.Restaurants.Add(yavorsPlace);

                List<Review> reviews = new List<Review>()
            {
                new Review()
                {
                     Restaurant = yavorsPlace,
                     Rating = 5,
                     Reviewer = yavor,
                     Text = "very good place to eat"
                },

                new Review()
                {
                     Restaurant = yavorsPlace,
                     Rating = 7,
                     Reviewer = yavor,
                     Text = "very good place to be"
                }
            };

                foreach (var review in reviews)
                {
                    context.Reviews.Add(review);
                }

                context.SaveChanges();
            }
        }
    }
}
